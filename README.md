# Rapport#

Le projet est un pockedeck, c'est-à-dire un outil qui permet de gérer ses cartes Pokemon lors d'une bataille Pokémon.

### Fonctionnalités ###

*Ajouter une carte
*Modifier une carte
*Supprimer une carte

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Mode opératoire ###

D'abord, on créée une carte du type Trainer ou Energy dans l'onglet "Add card"
La carte de type Pokemon dépend d'une carte Energy donc on la crée après une carte Energy.
Donc, on créée une carte en remplissant le formulaire. Un petit message de confirmation s'affiche.
Ensuite on peut 
*modifier la description d'une carte existante dans l'onglet "Edit card", en sélectionnant une carte, puis écrivant la nouvelle description
*supprimer une carte dans l'onglet "Delete card"

### Architecture design ###

-1 panneau de 4 onglets, 1 pour chaque fonctionnalité:
*Add card
*Edit card
*Delete card
*View cards
-Dans l'onglet "Add card", 1 panneau de 3 onglets, 1 pour chaque type de carte.

### Pourquoi design en panneaux ? ###
Je voulais faire s'afficher des formulaire au clic sur des boutons dans l'accueil par exemple: Add, Edit, Delete
Mais je n'ai pas réussi.

### Outils utilisés ###
Développé sur Eclipse, avec l'aide du plugin WindowBuilder pour plus de rapidité et facilité.
Dia pour dessiner l'UML

### UML ###
Disponible dans le fichier uml.png
J'ai fais une classe Card, héritée par les types de cartes (Pokemon, Trainer, Energy), et une classe Deck qui contient un tableau de cartes.