
public class Trainer extends Card 
{
	//properties
	private String textBox;
	private String trainerRule;
	//constructors
	public Trainer()
	{
		super();
		this.setName("Trainer Card");
		this.textBox="";
		this.trainerRule="";
	}
	public Trainer(String aTextbox, String aTrainerrule)
	{
		super();
		this.setName("Trainer Card");
		this.textBox=aTextbox;
		this.trainerRule=aTrainerrule;
	}
	//getters & setters
	public String getTextBox() {
		return textBox;
	}
	public void setTextBox(String textBox) {
		this.textBox = textBox;
	}
	public String getTrainerRule() {
		return trainerRule;
	}
	public void setTrainerRule(String trainerRule) {
		this.trainerRule = trainerRule;
	}
	
}
