public class Card 
{
	//properties
	private String name;
	//constructors
	public Card()
	{
		this.name="Unknown";
	}
	public Card(String aName)
	{
		this.name=aName;
	}
	//getters & setters
	public String getName()
	{
		return this.name;
	}
	public void setName(String aName)
	{
		this.name=aName;
	}
	//toString
	public String toString ()
	{
		return "Card name: "+name+"\n";
	}
}
