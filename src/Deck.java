import java.util.ArrayList;

public class Deck {
	//properties
	private ArrayList<Card> deck;
	//constructors
	public Deck()
	{
		this.setDeck(new ArrayList<Card>());
	}
	//methods
		//add a card in the deck
	public void addCard(Card aCard)
	{
		this.deck.add(aCard);
	}
		//remove a card from the deck
	public void removeCard(Card aCard)
	{
		this.deck.remove(aCard);
	}
	
	//getters & setters
	public ArrayList<Card> getDeck() {
		return this.deck;
	}
	public void setDeck(ArrayList<Card> deck) {
		this.deck = deck;
	}
	
}
