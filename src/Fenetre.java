import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout.Alignment;
public class Fenetre extends JFrame implements ActionListener {
	private JPanel contentPane;
	protected Window energyPanel;
	protected Window pokemonPanel;
	private JTextField nameInput;
	private JTextField nameEnergyInput;
	private  JButton btnAddEnergy;
	private JLabel addedConfirm;
	private Deck deck;
	private JButton btnAddPokemon ;
	private JComboBox comboBox;
	private  JSpinner pvInput;
	private JSpinner stageInput;
	private JTextArea descriptionInput;
	private JLabel confirmAddPokemon;
	private JTextArea trainerInput;
	private JTextArea textBoxInput;
	private JButton btnAddTrainer;
	private JComboBox comboCardsEditList;
	private JComboBox cardsDelList;
	private Panel editChosen;
	private Button editButton;
	private Label changeDescriptionLabel;
	private Button editSubmitDesc;
	private TextArea changeDescriptionInput;
	private Label confirmEditLabelDesc;
	/**
	 * Create the frame.
	 */
	public Fenetre() {
		deck=new Deck(); //initialize the deck
		setTitle("Pokedeck");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 666, 586);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(153, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		//panel
		JPanel panel = new JPanel();
		panel.setBackground(new Color(153, 0, 0));
		contentPane.add(panel, BorderLayout.CENTER);
		//jtabbedpane
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 620, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(67)
					.addComponent(tabbedPane, GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
					.addContainerGap())
		);		
	//add page
		//onglet
		JLayeredPane addItem = new JLayeredPane();
		tabbedPane.addTab("Add card", null, addItem, null);
		//container
		JTabbedPane addContainer = new JTabbedPane(JTabbedPane.TOP);
		addContainer.setBackground(new Color(255, 255, 204));
		addContainer.setBounds(108, 58, 426, 339);
		addItem.add(addContainer);
		
		JLayeredPane energyItem = new JLayeredPane();
		addContainer.addTab("Energy", null, energyItem, null);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(34, 34, 46, 14);
		energyItem.add(lblName);
		
		nameEnergyInput = new JTextField();
		nameEnergyInput.setBounds(98, 31, 86, 20);
		energyItem.add(nameEnergyInput);
		nameEnergyInput.setColumns(10);
		//Btn submit energy form
		btnAddEnergy = new JButton("Add");
		btnAddEnergy.setBounds(95, 162, 89, 23);
		btnAddEnergy.addActionListener(this); //Adding listener
		energyItem.add(btnAddEnergy);
		//confirm message in  form
		addedConfirm = new JLabel("Added !");
		addedConfirm.setFont(new Font("Tahoma", Font.BOLD, 11));
		addedConfirm.setBackground(new Color(0, 204, 0));
		addedConfirm.setBounds(114, 137, 46, 14);
		addedConfirm.setVisible(false);
		energyItem.add(addedConfirm); 
		//onglet add card type pokemon
		JLayeredPane pokemonItem = new JLayeredPane();
		addContainer.addTab("Pokemon", null, pokemonItem, null);
			//pv
		JLabel pvLabel = new JLabel("PV");
		pvLabel.setBounds(10, 48, 34, 14);
		pokemonItem.add(pvLabel);
			//pv-input
		pvInput = new JSpinner();
		pvLabel.setLabelFor(pvInput);
		pvInput.setBounds(105, 45, 40, 20);
		pokemonItem.add(pvInput);
			//name
		JLabel nameLabel = new JLabel("Name");
		nameLabel.setBounds(10, 21, 46, 14);
		pokemonItem.add(nameLabel);
			//name-input
		nameInput = new JTextField();
		nameLabel.setLabelFor(nameInput);
		nameInput.setBounds(105, 14, 134, 20);
		pokemonItem.add(nameInput);
		nameInput.setColumns(10);
			//stage
		JLabel stageLabel = new JLabel("Stage");
		stageLabel.setBounds(10, 85, 46, 14);
		pokemonItem.add(stageLabel);
			//stage-input
		stageInput = new JSpinner();
		stageLabel.setLabelFor(stageInput);
		stageInput.setBounds(105, 76, 40, 20);
		pokemonItem.add(stageInput);
			//description
		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(10, 112, 85, 14);
		pokemonItem.add(lblDescription);
			//description-input
		descriptionInput = new JTextArea();
		descriptionInput.setForeground(new Color(0, 0, 0));
		descriptionInput.setBounds(105, 107, 134, 29);
		pokemonItem.add(descriptionInput);
			//energyType
		JLabel lblNewLabel = new JLabel("Energy type");
		lblNewLabel.setBounds(10, 153, 85, 14);
		pokemonItem.add(lblNewLabel);
			//energyType-input
		comboBox = new JComboBox();
		comboBox.setBounds(105, 147, 134, 20);
		pokemonItem.add(comboBox);		
		//buttn submit add pokemon form
		btnAddPokemon = new JButton("Add");
		btnAddPokemon.setBounds(76, 214, 89, 23);
		btnAddPokemon.addActionListener(this);
		pokemonItem.add(btnAddPokemon);
		//  Pokemon Add confirm messaege
		 confirmAddPokemon = new JLabel("Added!");
		confirmAddPokemon.setBounds(160, 191, 46, 14);
		confirmAddPokemon.setVisible(false);
		pokemonItem.add(confirmAddPokemon);
	//trainer card	
		JLayeredPane trainerItem = new JLayeredPane();
		addContainer.addTab("Trainer", null, trainerItem, null);
		
		JLabel lblTextBox = new JLabel("Text Box");
		lblTextBox.setBounds(10, 28, 64, 14);
		trainerItem.add(lblTextBox);
		
		textBoxInput = new JTextArea();
		textBoxInput.setBounds(105, 23, 101, 45);
		trainerItem.add(textBoxInput);
		
		JLabel lblTrainerRule = new JLabel("Trainer rule");
		lblTrainerRule.setBounds(10, 82, 74, 14);
		trainerItem.add(lblTrainerRule);
		
		trainerInput = new JTextArea();
		trainerInput.setBounds(105, 79, 101, 69);
		trainerItem.add(trainerInput);
		
		btnAddTrainer = new JButton("Add");
		btnAddTrainer.setBounds(55, 171, 89, 23);
		btnAddTrainer.addActionListener(this);
		trainerItem.add(btnAddTrainer);
		
		JLayeredPane delItem = new JLayeredPane();
		tabbedPane.addTab("Delete card", null, delItem, null);
		
		JLabel lblSelectTheCard = new JLabel("Select the card you want to delete");
		lblSelectTheCard.setBounds(10, 52, 238, 14);
		delItem.add(lblSelectTheCard);
		
		cardsDelList = new JComboBox();
		lblSelectTheCard.setLabelFor(cardsDelList);
		cardsDelList.setBounds(258, 49, 154, 20);
		delItem.add(cardsDelList);
		
		Button deleteCard = new Button("Delete");
		deleteCard.setBounds(258, 125, 70, 22);
		delItem.add(deleteCard);
		
		JLayeredPane viewItem = new JLayeredPane();
		tabbedPane.addTab("View cards", null, viewItem, null);
		
		JLayeredPane editItem = new JLayeredPane();
		tabbedPane.addTab("Edit card", null, editItem, null);
		
		JLabel lblSelectTheCardEdit = new JLabel("Select the card you want to edit");
		lblSelectTheCardEdit.setBounds(10, 53, 203, 14);
		editItem.add(lblSelectTheCardEdit);
		
		comboCardsEditList = new JComboBox();
		lblSelectTheCardEdit.setLabelFor(comboCardsEditList);
		comboCardsEditList.setBounds(223, 50, 196, 20);
		editItem.add(comboCardsEditList);
		
		editChosen = new Panel();
		editChosen.setBounds(94, 112, 397, 242);
		editChosen.setVisible(false);
		editItem.add(editChosen);
		
		changeDescriptionLabel = new Label("New label");
		editChosen.add(changeDescriptionLabel);
		
		changeDescriptionInput = new TextArea();
		editChosen.add(changeDescriptionInput);
		
		editSubmitDesc = new Button("Ok");
		editSubmitDesc.addActionListener(this);
		editChosen.add(editSubmitDesc);
		
		confirmEditLabelDesc = new Label("Edited!");
		confirmEditLabelDesc.setVisible(false);
		editChosen.add(confirmEditLabelDesc);
		
		editButton = new Button("Edit");
		editButton.setBounds(173, 143, 70, 22);
		editItem.add(editButton);
		editButton.addActionListener(this);
		panel.setLayout(gl_panel);
	}
	/*Actions*/
	public void actionPerformed(ActionEvent e) 
	{	
		Object source=e.getSource();
		//Add Energy Card
		if(source==btnAddEnergy)
		{
			Energy newEnergy=new Energy(nameEnergyInput.getText()); //Creation energy card
			deck.addCard(newEnergy); //Add energy card to the deck
			nameEnergyInput.setText("");
			addedConfirm.setVisible(true);//Enable the confirm message
			//adding new energy card to the energy type combobox of adding pokemon page
			comboBox.addItem(newEnergy.getName());		
			comboCardsEditList.addItem(newEnergy.getName());
			cardsDelList.addItem(newEnergy.getName());
		}
		//Add Pokemon Card
		if(source == btnAddPokemon)
		{
			int pv = (Integer) pvInput.getValue();
			String name = nameInput.getText();
			int stage = (Integer) stageInput.getValue();
			String description = descriptionInput.getText();
			String energy = (String) comboBox.getSelectedItem();
			Energy energyType=null;
			//to convert var energy to object Energy
			ArrayList<Card>cards=deck.getDeck(); //all cards
			for(Card acard:cards)
			{
				if(acard instanceof Energy) //just get energy cards
				{
					if(acard.getName()==energy)
					{
						energyType=(Energy) acard;
						break;
					}
				}
			}
			Pokemon newPoke=new Pokemon(pv, name, stage, description, energyType);//persist new pokemon card
			confirmAddPokemon.setVisible(true); //Enable confirm msg
			comboCardsEditList.addItem(newPoke.getName());
			cardsDelList.addItem(newPoke.getName());
			
		}
		else if(source==btnAddTrainer)//new Trainer card
		{
			Trainer newTrainerCard= new Trainer(textBoxInput.getText(),	trainerInput.getText());
			comboCardsEditList.addItem(newTrainerCard.getName());
			cardsDelList.addItem(newTrainerCard.getName());
		}
		else if(source==editButton)
		{
			editChosen.setVisible(true);
			changeDescriptionLabel.setText(comboCardsEditList.getName()); //this does not work
		}
		else if(source==editSubmitDesc)
		{
			String description=changeDescriptionInput.getText();
			Card card;
			String name=changeDescriptionLabel.getText();
			ArrayList<Card>cards=deck.getDeck(); //all cards
			Pokemon pok=null;
			Integer i=null;
			for(Card acard:cards)
			{
				if(acard instanceof Pokemon) //just get energy cards
				{
					if(acard.getName()==name)
					{
						i=cards.indexOf(acard);
						pok=(Pokemon) acard;
						break;
					}
				}
			}
			if(i!=null)
			{
				Pokemon pok2=pok;
				pok.setDescription(description);
				((List<Card>) deck).set(i, pok2); //change Pokemon with index i by pok2 (the edited one)
			}
		}
		else if(source==editSubmitDesc)
		{
			confirmEditLabelDesc.setVisible(true); //this does not work
		}
	}
}
