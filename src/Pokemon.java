public class Pokemon extends Card 
{
	//properties
	private String uname;
	private int pv;
	private int  stage;
	private String description;
	private Energy energyType;
	private Pokemon pokemonEnvolvedFrom;
	//constructors
	public Pokemon()
	{
		super();
		this.stage=0;
		this.description="unknown";
		this.energyType=null;
		this.setPokemonEnvolvedFrom(null);
		this.uname=this.getName();
	}

	public Pokemon(int aPv, String aName, int aStage, String aDescription, Energy anenergy)
	{
		super();
		this.pv=aPv;
		this.setName(aName);
		this.stage=aStage;
		this.description=aDescription;
	}
	//getters & setters
	public int getStage() {
		return stage;
	}
	public void setStage(int stage) {
		this.stage = stage;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getPv() {
		return pv;
	}
	public void setPv(int pv) {
		this.pv = pv;
	}
	public Energy getEnergyType() {
		return energyType;
	}
	public void setEnergyType(Energy energyType) {
		this.energyType = energyType;
	}
	public Pokemon getPokemonEnvolvedFrom() {
		return pokemonEnvolvedFrom;
	}
	public void setPokemonEnvolvedFrom(Pokemon pokemonEnvolvedFrom) {
		this.pokemonEnvolvedFrom = pokemonEnvolvedFrom;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String toString()
	{
		return super.toString()+"Pokemon name: ";
	}
}
